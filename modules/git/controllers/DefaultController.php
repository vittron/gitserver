<?php

namespace app\modules\git\controllers;

use app\modules\git\components\GitForm;
use Cz\Git\GitRepository;
use Yii;
use yii\data\ArrayDataProvider;
use yii\web\Controller;

/**
 * Default controller for the `git` module
 */
class DefaultController extends Controller
{
    public $repoWebPath;

    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        $model = new GitForm();
        $repoBranches = [];

        if (Yii::$app->request->isPost) {
            $model->load(\Yii::$app->request->post());
            $gitURL = $model->repoWebPath;
            preg_match('/\/([a-zA-Z0-9._-]+)\.git/u', $gitURL, $repoName);
            $repoName = $repoName[1];
            $repoPath = Yii::getAlias('@app/uploads/') . $repoName;
            if (!file_exists($repoPath)) {
                $repo = GitRepository::cloneRepository($gitURL, $repoPath);
                Yii::$app->session->setFlash('success', \Yii::t('app', 'Repository was cloned successfully.'));
            } else {
                $repo = new GitRepository($repoPath);
                Yii::$app->session->setFlash('warning', \Yii::t('app', 'Repository exists!'));
            }
            $repoBranches = $repo->getBranches();
        }

        return $this->render('index', [
            'model' => $model,
            'repoBranches' => new ArrayDataProvider([
                'allModels' => $repoBranches,
                'pagination' => false,
            ]),
        ]);
    }
}