<?php

namespace app\modules\git\components;

use yii\base\Model;

class GitForm extends Model
{
    public $repoWebPath;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['repoWebPath'], 'required'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'repoWebPath' => \Yii::t('app', 'Repository URL'),
        ];
    }
}