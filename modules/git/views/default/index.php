<?php
use yii\widgets\ActiveForm;
use yii\widgets\ActiveField;
use yii\helpers\Html;
?>

<?php $form = ActiveForm::begin() ?>

    <?= $form->field($model, 'repoWebPath')->textInput(); ?>

    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Clone', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>

<?php ActiveForm::end() ?>

<?php if (isset($repoBranches)) : ?>
<div class="repository-branches">
    <h2>Repository Branches:</h2>
    <?= \yii\widgets\ListView::widget([
        'dataProvider' => $repoBranches,
        'itemView' => '_branch',
    ]); ?>
</div>
<?php endif; ?>