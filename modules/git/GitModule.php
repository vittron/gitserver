<?php

namespace app\modules\git;

use Yii;
use yii\base\Event;
use yii\web\User;
use yii\base\Module;
use yii\base\Response;

/**
 * Module that contains git functionality
 */
class GitModule extends Module
{
    /**
     * Init module
     */
    public function init()
    {
        parent::init();
    }
}